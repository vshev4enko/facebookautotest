const { generateEmail, getInbox, deleteMail } = require('temp-mail/index');

function MailHelper() {
  this._mail = null;
  this._message = null;
  this._mailGenerator();
}

MailHelper.prototype = {

  _mailConf: function(mail) {
    this._mail = mail;
    this._getMessages;
  },

  _mailGetter: function(message) {
    this._message = message
  },

  _mailGenerator: function() {
    var generator = generateEmail();
    generator
      .then(
        result => {
          this._mailConf(result);
        },
        error => {
          console.log("mail generator error "+error)
        })
  },

  _getMessages: function() {
    var getBox = new getInbox(this._mail);
    getBox
      .then(
        result => {
          this._mailGetter(result);
        },
        error => {
          console.log("getMessages error "+error)
        })
  },

  getMailBox: function() {
    return this._mail;
  },

  getMessages: function() {
    return this._message;
  },
};

module.exports = exports = MailHelper;