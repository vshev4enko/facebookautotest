var assert = require('assert');
var until = require('selenium-webdriver').until;
var By = require('selenium-webdriver').By;

var loginLocator = By.xpath('//*[@type="email"]');
var passwordLocator = By.xpath('//*[@type="password"]');
var loginButtonLocator = By.xpath('//*[@type="submit"]');
var menuBarLocator = By.css('.loggedout_menubar_container');
var logoLocator = By.css('.fb_logo');
var naviBarLocator = By.xpath('//*[@id="u_0_a"]');
var userNaviLabel = By.xpath('//*[@id="userNavigationLabel"]');


var Component = function (driver) {
	this._driver = driver;
};

Component.prototype.inputLogin = function(text) {
	this._driver.findElement(loginLocator).sendKeys(text)
	return this
}

Component.prototype.inputPass = function(text) {
	this._driver.findElement(passwordLocator).sendKeys(text)
	return this
}

Component.prototype.pressLog_in = function() {
	this._driver.findElement(loginButtonLocator).click()
	return this
}


Component.prototype._getMenuBarBlock = function(locator) {
	var driver = this._driver;
	return driver.wait(until.elementLocated(locator), 10000)
		.then(function (result) {
			if (!result) {
				throw new Error('Menubar component is not found');
			}
			return driver.findElement(locator);
		});
};

Component.prototype.pageValidate = function() {
	return this._getMenuBarBlock(menuBarLocator)
		.then(function(block) {
			var title = block.findElement(logoLocator);
			return title.getText()
				.then(function(text) {
					assert.equal(text, 'Facebook');
					return true;
				});
		});
};

Component.prototype.loginValidate = function() {
	return this._getMenuBarBlock(naviBarLocator)
		.then(function(block) {
			var title = block.findElement(userNaviLabel);
			return title.getText()
				.then(function(text) {
					assert.equal(text, 'Account Settings');
					return true;
				})
		})
}

module.exports = Component;