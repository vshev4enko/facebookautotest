var assert = require('assert');
var until = require('selenium-webdriver').until;
var By = require('selenium-webdriver').By;

var firstNameFieldLocator = By.xpath('//*[@id="u_0_g"]');
var secondNameFieldLocator = By.xpath('//*[@id="u_0_i"]');
var mailFieldLocator = By.xpath('//*[@id="u_0_l"]');
var mailRepeatLocator = By.xpath('//*[@id="u_0_o"]');
var passwordFieldLocator = By.xpath('//*[@id="u_0_s"]');
var genderMaleSelect = By.xpath('/descendant::input[@name="sex"][1]');
var genderFemaleSelect = By.xpath('/descendant::input[@name="sex"][2]');
var createAccountButton = By.xpath('//*[@id="u_0_y"]');
var yesButtonLocator = By.css('button._42ft');
var popUpApprove = By.css('._4-i0');



var Component = function (driver) {
	this._driver = driver;
};

Component.prototype.inputFirstNameField = function(text) {
	this._driver.findElement(firstNameFieldLocator).sendKeys(text)
	return this
}

Component.prototype.inputSecondNameField = function(text) {
	this._driver.findElement(secondNameFieldLocator).sendKeys(text)
	return this
}

Component.prototype.inputMailField = function(text) {
	this._driver.findElement(mailFieldLocator).sendKeys(text)
	return this
}

Component.prototype.inputMailRepeatField = function(text) {
	this._driver.findElement(mailRepeatLocator).sendKeys(text)
	return this
}

Component.prototype.inputPassField = function(text) {
	this._driver.findElement(passwordFieldLocator).sendKeys(text)
	return this
}

Component.prototype.genderMaleSelect = function() {
	this._driver.findElement(genderMaleSelect).click()
	return this
}

Component.prototype.genderFemaleSelect = function() {
	this._driver.findElement(genderFemaleSelect).click()
	return this
}

Component.prototype.createAccountButtonPress = function() {
	this._driver.findElement(createAccountButton).click()
	return this
}

Component.prototype.getApproveBlock = function() {
	var driver = this._driver;
	return driver.wait(until.elementLocated(popUpApprove), 10000)
		.then(function (result) {
			if (!result) {
				throw new Error('Menubar component is not found');
			}
			return driver.findElement(popUpApprove);
		});
};

Component.prototype.popUpApprovingMail = function() {
	this._driver.findElement(popUpApprove).click()
	return this
}

Component.prototype.yesButtonPress = function() {
	this._driver.findElement(yesButtonLocator).click()
	return this
}


module.exports = Component;