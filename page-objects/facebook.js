var until = require('selenium-webdriver').until;
var promise = require('selenium-webdriver').promise;
var By = require('selenium-webdriver').By;


var Page = function (driver) {
	this._driver = driver;
};

Page.prototype.url = 'https://facebook.com';

Page.prototype.open = function (condition) {
	this._driver.get(this.url);
	return this;
};

Page.prototype.getLoginComponent = function() {
	var Component = require('./login-component');
	return new Component(this._driver);
};

Page.prototype.getRegisterComponent = function() {
	var Component = require('./register-component');
	return new Component(this._driver);
};

Page.prototype.waitReady = function(condition, timeout) {
	if (condition) {
		timeout = timeout || 10 * 1000;
		this._driver.wait(until.elementLOcated(condition), timeout);
	}
	return this;
};

Page.prototype.getReadyCondition = function(locator) {
	return until.elementLocated(locator);
};

Page.prototype.validate = function () {
	return promise
		.all([
			this.getMenuComponent().validate(),
			this.getWeatherComponent().validate()
		])
		.then(function (results) {
			return results[0] && results[1];
		});
};

module.exports = Page;