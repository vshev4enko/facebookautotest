var webdriver = require('selenium-webdriver');
var promise = webdriver.promise;


function DriverProvider() {
	this._driver = null;
}

DriverProvider.prototype = {

	buildDriver: function() {
		this._driver = new webdriver.Builder()
			.forBrowser(webdriver.Browser.FIREFOX)
			.build();
		if (!this._driver) {
			throw new Error('Unable to build driver');
		}
		return promise.fulfilled(this._driver);
	},

	destroyDriver: function() {
		return this._driver ?
			this._driver.quit() :
			promise.rejected(new Error('No driver was found'));
	},

	getDriver: function() {
		return this._driver;
	},

	startUp: function() {
		var flow = promise.controlFlow();
		return promise.all([
			flow.execute(this.buildDriver.bind(this))
		]);
	},

	tearDown: function() {
		var flow = promise.controlFlow();
		return promise.all([
			flow.execute(this.destroyDriver.bind(this))
		]);
	}
};

module.exports = DriverProvider;