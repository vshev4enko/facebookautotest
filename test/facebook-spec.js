const mailHelper = require('../helper/mail-helper');
var DriverProvider = require('../driver-provider');
var assert = require('assert');
var test = require('../node_modules/selenium-webdriver/testing');
var Page = require('../page-objects/facebook');

var email = "juhoxo@p33.org";
var pass = "amy@1986";
var test_name = "abracadabra";

test.describe('facebook open start page test', function() {
	var driverProvider = new DriverProvider();
	this.timeout(20000);

	test.before(function () {
		return driverProvider.startUp();
	});

	test.after(function () {
		return driverProvider.tearDown();
	});

	test.it('Should be a valid', function() {
		var driver = driverProvider.getDriver();
		var page = new Page(driver);
		return page
			.open(page.waitReady(page.logoLocator))
			.getLoginComponent()
			.pageValidate()
	});

	test.describe('facebook auth login test', function() {

		test.it('Should be a logon', function() {
			var driver = driverProvider.getDriver();
			var page = new Page(driver);
			return page
				.open(page.waitReady(page.loginLocator))
				.getLoginComponent()
				.inputLogin(email)
				.inputPass(pass)
				.pressLog_in()
				.loginValidate()
		});
	});
});


test.describe('facebook registration', function() {
	this.timeout(15000);
	var mail = new mailHelper();
	var driverProvider = new DriverProvider();

	test.before(function () {
		return driverProvider.startUp();
	});

	test.after(function () {
		return driverProvider.tearDown();
	});

	test.it('Should registration', function(done) {
		var driver = driverProvider.getDriver();
		var page = new Page(driver);
		var mailLetter;
		var mailBox = mail.getMailBox();
		return page
			.open(page.waitReady(page.logoLocator))
			.getRegisterComponent()
			.inputFirstNameField(test_name)
			.inputSecondNameField(test_name)
			.inputMailField(mailBox)
			.inputMailRepeatField(mailBox)
			.inputPassField(pass)
			.genderFemaleSelect()
			.createAccountButtonPress()
			.yesButtonPress()
			
	});
});