This code requires:

1. Node.js
2. Python 2.7.x
3. Java Runtime Environment
4. [mocha](http://mochajs.org/)
5. [selenium-webdriver](https://www.npmjs.com/package/selenium-webdriver)

for run:
needs have geckodriver in PATH
git clone git@bitbucket.org:vshev4enko/facebookautotest.git
cd facebookautotest
npm install 
npm test